{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- title: Python operators and how they affect pandas\n",
    "- tags: pandas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python is today among the most popular programming languages. And in my opinion, there is one main reason for it, **readability**.\n",
    "\n",
    "A clear example of how Python was designed to be readable is next example:\n",
    "\n",
    "```python\n",
    "if 'melon' not in ('apple', 'coconut'): print('it is missing!')\n",
    "```\n",
    "\n",
    "And compare this to for example Javascript:\n",
    "\n",
    "```javascript\n",
    "var fruits = new Array(\"apple\", \"coconut\");\n",
    "if (!(fruits.indexOf(\"melon\") > 0)) { console.log(\"it is missing!\"); }\n",
    "```\n",
    "\n",
    "I think it's clear how Python is trying to make humans life easy, even at the cost of extra complexity in the interpreter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example\n",
    "\n",
    "The above comment about readability is not only true when you are writing code that you need to read later. It also applies when you are programming libraries,\n",
    "that users will use. Python is also designed to let you write libraries in a way that your users will be able to write readable code.\n",
    "\n",
    "For example, let's think of a toy library that implements colors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<span style=\"color: #0000ff\">▅</span>"
      ],
      "text/plain": [
       "<__main__.ColorStep1 at 0x7f99bd7ed7b8>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class ColorStep1:\n",
    "    def __init__(self, red=0, green=0, blue=0):\n",
    "        self.red = red\n",
    "        self.green = green\n",
    "        self.blue = blue\n",
    "\n",
    "    def __str__(self):\n",
    "        \"\"\"Convert the color from the 3 integer values, to a string like #ffffff.\"\"\"\n",
    "        return f'#{self.red:02x}{self.green:02x}{self.blue:02x}'\n",
    "\n",
    "    def _repr_html_(self):\n",
    "        \"\"\"Display the color as a box of its color in Jupyter.\"\"\"\n",
    "        return f'<span style=\"color: {self}\">▅</span>'\n",
    "\n",
    "blue = ColorStep1(blue=255)\n",
    "blue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_**Note**: In practice it would make sense to have a single `Color` class with all the methods. I'll be writing it in separate `ColorStepN` classes that inherit from the previous to show the development step by step._\n",
    "\n",
    "A common way to mix colors could be to simply implement a `mix` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<span style=\"color: #ffff00\">▅</span>"
      ],
      "text/plain": [
       "<__main__.ColorStep2 at 0x7f99bd7edd68>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class ColorStep2(ColorStep1):\n",
    "    @staticmethod\n",
    "    def _mix_one(color1, color2):\n",
    "        \"\"\"There are many ways to mix colors, here we just take the sum of the components.\"\"\"\n",
    "        return min(color1 + color2, 255)\n",
    "\n",
    "    def mix(self, other):\n",
    "        return ColorStep2(red=self._mix_one(self.red, other.red),\n",
    "                          green=self._mix_one(self.green, other.green),\n",
    "                          blue=self._mix_one(self.blue, other.blue))\n",
    "\n",
    "red = ColorStep2(red=255)\n",
    "green = ColorStep2(green=255)\n",
    "\n",
    "# Mixing red and green to generate yellow\n",
    "red.mix(green)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works well, but could we make that last line more readable? I think so. I think it would be really cool for the users\n",
    "of our colors library if they could simply write `red + green`.\n",
    "\n",
    "As mentioned before, **Python is designed to not only let us write readable code, but to write libraries that will make the code of our users readable**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operators\n",
    "\n",
    "A first version of our class with operators could look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<span style=\"color: #ffff00\">▅</span>"
      ],
      "text/plain": [
       "<__main__.ColorStep2 at 0x7f99bd7edcc0>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class ColorStep3(ColorStep2):\n",
    "    def __add__(self, other):\n",
    "        return self.mix(other)\n",
    "\n",
    "red = ColorStep3(red=255)\n",
    "green = ColorStep3(green=255)\n",
    "\n",
    "red + green"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can disagree, but to me, and I bet to most Python programmers, `red + green` is easier to read than `red.mix(green)`.\n",
    "So, we managed to let users use this syntax, with just the addition of the special `__add__` method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interacting with other types\n",
    "\n",
    "An extra feature that I would like to have, is to be able to mix my color class, with colors in the form `#ffffff`.\n",
    "Let's give it a try first, and see why it fails:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'str' object has no attribute 'red'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-4-a9a4578f859e>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0mgreen\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m'#00ff00'\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      3\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 4\u001b[0;31m \u001b[0mred\u001b[0m \u001b[0;34m+\u001b[0m \u001b[0mgreen\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-3-354fcff4f7c5>\u001b[0m in \u001b[0;36m__add__\u001b[0;34m(self, other)\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;32mclass\u001b[0m \u001b[0mColorStep3\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mColorStep2\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0m__add__\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mother\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mmix\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mother\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      4\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      5\u001b[0m \u001b[0mred\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mColorStep3\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mred\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m255\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-2-828cfc83e29c>\u001b[0m in \u001b[0;36mmix\u001b[0;34m(self, other)\u001b[0m\n\u001b[1;32m      6\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      7\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0mmix\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mother\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 8\u001b[0;31m         return ColorStep2(red=self._mix_one(self.red, other.red),\n\u001b[0m\u001b[1;32m      9\u001b[0m                           \u001b[0mgreen\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_mix_one\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgreen\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mother\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgreen\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     10\u001b[0m                           blue=self._mix_one(self.blue, other.blue))\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'str' object has no attribute 'red'"
     ]
    }
   ],
   "source": [
    "red = ColorStep3(red=255)\n",
    "green = '#00ff00'\n",
    "\n",
    "red + green"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our implementation of the `mix` method is assuming that we'll receive an instance of the color class.\n",
    "Since it expects to find the attributes `red`, `green` and `blue`.\n",
    "\n",
    "What we will do is to create a method to convert the string representation to our class.\n",
    "And then we will automatically convert the `other` parameter if it is a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<span style=\"color: #ffff00\">▅</span>"
      ],
      "text/plain": [
       "<__main__.ColorStep2 at 0x7f99bcfa5390>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class ColorStep4(ColorStep3):\n",
    "    @staticmethod\n",
    "    def _parse_rgb_string(value):\n",
    "        import re\n",
    "        match = re.search('^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$',\n",
    "                          value.lower())\n",
    "        \n",
    "        return ColorStep4(red=int(match.group(1), 16),\n",
    "                          green=int(match.group(2), 16),\n",
    "                          blue=int(match.group(3), 16))\n",
    "\n",
    "    def __add__(self, other):\n",
    "        if isinstance(other, str):\n",
    "            other = self._parse_rgb_string(other)\n",
    "\n",
    "        return self.mix(other)\n",
    "\n",
    "red = ColorStep4(red=255)\n",
    "green = '#00ff00'\n",
    "\n",
    "red + green"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This wasn't difficult. Now I can add (mix) a string to my color class. But can I add my color class to a string?\n",
    "Python strings are generic, and they don't know anything about the color class I just implemented.\n",
    "\n",
    "The answer is no:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "can only concatenate str (not \"ColorStep4\") to str",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-6-81eb708d4cba>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0mgreen\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mColorStep4\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mgreen\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;36m255\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      3\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 4\u001b[0;31m \u001b[0mred\u001b[0m \u001b[0;34m+\u001b[0m \u001b[0mgreen\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: can only concatenate str (not \"ColorStep4\") to str"
     ]
    }
   ],
   "source": [
    "red = '#ff0000'\n",
    "green = ColorStep4(green=255)\n",
    "\n",
    "red + green"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I don't think it makes sense to modify the `str` type in Python to let it know about our new class (and it wouldn't be simple).\n",
    "\n",
    "Luckily, Python provides a way to let this happen easily. The idea is that after the operation raises the `TypeError` exception,\n",
    "and before it is reported to the user, Python will try something else. Will try to find a `__radd__` method in the class at the\n",
    "right of the operation. In this case it didn't exist, but let's see what happens if we implement it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<span style=\"color: #ffff00\">▅</span>"
      ],
      "text/plain": [
       "<__main__.ColorStep2 at 0x7f99bd7ed198>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "class ColorStep5(ColorStep4):\n",
    "    def __radd__(self, other):\n",
    "        return self + other\n",
    "\n",
    "red = '#ff0000'\n",
    "green = ColorStep5(green=255)\n",
    "\n",
    "red + green"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And volià, it worked. :)\n",
    "\n",
    "What happened here is next:\n",
    "- We tried the operation `add` with `str + color`\n",
    "- Python called the `__add__` method of `str`, and it raised `TypeError`\n",
    "- Then Python captured the error, and called the `__radd__` of `color`, with the `str` instance as the `other` parameter\n",
    "- That worked, and Python reported the result to the user"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Limitations\n",
    "\n",
    "This is great, and we can not only operate our class with additions from and to any other class, but there are many other operations we can do.\n",
    "Just some random examples:\n",
    "- `color + color`\n",
    "- `color + whatever`\n",
    "- `whatever + color`\n",
    "- `color - color`\n",
    "- `color * whatever`\n",
    "- `whatever == color`\n",
    "- `color > color`\n",
    "- ...\n",
    "\n",
    "The [Python documentation](https://docs.python.org/3/library/operator.html#mapping-operators-to-functions)\n",
    "has the full list of Python operators.\n",
    "\n",
    "This is great, but there are some operators that are not in this list:\n",
    "- `color and color`\n",
    "- `color or color`\n",
    "- `not color`\n",
    "- `color in (color1, color2)`\n",
    "\n",
    "There was a [proposal](https://www.python.org/dev/peps/pep-0335/) to be able to overload them,\n",
    "which [was rejected by Guido van Rossum](https://mail.python.org/pipermail/python-dev/2012-March/117510.html).\n",
    "\n",
    "While I don't know what are the implications for the interpreter of accepting the proposal (in terms of performance, complexity...),\n",
    "I do know what is the implication for library authors, and specifically to [pandas](https://pandas.pydata.org/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Operators in pandas\n",
    "\n",
    "pandas makes heavy use of operation overloading. See these examples:\n",
    "\n",
    "```python\n",
    "df['distance_in_miles'] * 1.609344\n",
    "\n",
    "df['base_price'] + df['base_price'] * df['vat_rate']\n",
    "\n",
    "df['age'] >= 18\n",
    "```\n",
    "\n",
    "Now consider this other example:\n",
    "\n",
    "```python\n",
    "df['airline'] == 'DL' and not df['first_class']\n",
    "```\n",
    "\n",
    "While this looks very readable, there is a problem with this. The `and` and `not` operators are not being overloaded by pandas,\n",
    "since this is not allowed. So, they are the original operators from the Python interpreter.\n",
    "\n",
    "The original `and` and `not` operators will convert their parameters to a boolean value, and then evaluate the condition based on\n",
    "that. So, in this case `df['airline'] == 'DL'` won't be evaluated to one value per row, but converted to a single value `True` or `False`.\n",
    "This is not what a pandas user would expect, and it's inconsistent with the other operators, so this is not the syntax used by pandas.\n",
    "\n",
    "If pandas maintainers can't offer the above syntax, what are the alternatives? There are in my opinion two reasonable approaches.\n",
    "\n",
    "The **first solution** is to go back to using methods, like we started with `mix`. This would look like:\n",
    "\n",
    "```python\n",
    "pandas.and(df['airline'] == 'DL',\n",
    "           pandas.not(df['first_class']))\n",
    "```\n",
    "\n",
    "This is not valid Python syntax, since `and` and `not` are reserved keywords in Python, and will result in a syntax error.\n",
    "\n",
    "The recommended solution based on [PEP-8](https://www.python.org/dev/peps/pep-0008/#descriptive-naming-styles) is to add a\n",
    "_single trailing underscore_, so the final syntax would be:\n",
    "\n",
    "```python\n",
    "pandas.and_(df['airline'] == 'DL',\n",
    "            pandas.not_(df['first_class']))\n",
    "```\n",
    "\n",
    "I think we will all agree that is less readable than using the `and` and `not` operators.\n",
    "\n",
    "A **second solution** is to use other operators that we can overload. There are few that don't have an immediate\n",
    "use for dataframes, that can be considered. In particular, the bitwise operators.\n",
    "\n",
    "Let's have a look at the original bitwise operators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "binary and: 0010 & 1010 = 0010\n",
      "binary or: 0010 | 1010 = 1010\n",
      "binary inverse: ~ 0010 = 1101\n"
     ]
    }
   ],
   "source": [
    "binary_value_1 = 0b0010\n",
    "\n",
    "binary_value_2 = 0b1010\n",
    "\n",
    "result_and = binary_value_1 & binary_value_2\n",
    "\n",
    "result_or = binary_value_1 | binary_value_2\n",
    "\n",
    "result_not = ~ binary_value_1\n",
    "\n",
    "print(f'binary and: {binary_value_1:04b} & {binary_value_2:04b} = {result_and:04b}')\n",
    "print(f'binary or: {binary_value_1:04b} | {binary_value_2:04b} = {result_or:04b}')\n",
    "print(f'binary inverse: ~ {binary_value_1:04b} = {result_not & 0b1111:04b}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python provides these operators to opearte at the bit level. The `&` operator is like an `and`, but it doesn't operate for the\n",
    "whole value, but for each individual bit of it. We can see in the result, that there is a `1` in the positions where there is a\n",
    "`1` in the first value **and** in the second. The `or` operator is equivalent, there is a `1` where there is a `1` in the first\n",
    "value **or** there is a `1` in the second.\n",
    "\n",
    "Finally, the inverse just reverses every `0` and makes it a `1`, and the other way round.\n",
    "\n",
    "In pandas, initially, there was not much use for those, in the original meaning. So, they could be borrowed as the `and`, `or` and `not`\n",
    "operators for dataframes (or series).\n",
    "\n",
    "The result with the previous example would look like:\n",
    "\n",
    "```python\n",
    "df['airline'] == 'DL' & ~ df['first_class']\n",
    "```\n",
    "\n",
    "This looks correct, and this syntax is the one supported by pandas, but this is not equivalent to:\n",
    "\n",
    "```python\n",
    "df['airline'] == 'DL' and not df['first_class']\n",
    "```\n",
    "\n",
    "It is not because of [Python operator precedence](https://docs.python.org/3/reference/expressions.html#operator-precedence). This is\n",
    "the order in which operators are evaluated.\n",
    "\n",
    "See this example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "1 + 2 * 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If operators were evaluated from left to right, the previous result would be `1 + 2 = 3` and then `3 * 3 = 9`.\n",
    "But the `2 * 3` is actually happening first.\n",
    "\n",
    "Something similar is happening with the previous example.\n",
    "\n",
    "We would expect that the first to evaluate is:\n",
    "\n",
    "```python\n",
    "df['airline'] == 'DL'\n",
    "```\n",
    "\n",
    "And once this is computed, the `and` is performed with the second part of the expression (the condition on not being first class).\n",
    "This is what it would actually happen when using the Python `and` operator. But the bitwise `&` has a difference precedence.\n",
    "\n",
    "So how thinigs are actually being executed are:\n",
    "\n",
    "```python\n",
    "df['airline'] == ('DL' & (~ df['first_class']))\n",
    "```\n",
    "\n",
    "So, the `and` is not performed between the two conditions, but between `DL` and the second condition. This makes pandas conditions very\n",
    "tricky. And it's easy to get unexpected results.\n",
    "\n",
    "The solution for pandas is to be explicit on the order by using brackets:\n",
    "\n",
    "```python\n",
    "(df['airline'] == 'DL') & (~ df['first_class'])\n",
    "```\n",
    "\n",
    "This will ensure that the order in which operators are evaludated is the expected.\n",
    "\n",
    "I understand why pandas was designed this way, and I see value on having a more compact representation of conditions.\n",
    "But this feels quite hacky and counter-intuitive, and I would personally prefer the syntax presented before:\n",
    "\n",
    "```python\n",
    "pandas.and_(df['airline'] == 'DL',\n",
    "            pandas.not_(df['first_class']))\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
