# datapythonista.me

https://datapythonista.me

This is the personal page of Marc Garcia
[@datapythonista](https://datapythonista.me).

It has two main components:
- Home page 
  - `index.html`
  - `static`
  - `docs`: documents to share, like the CV
- Blog using [Pelican](https://blog.getpelican.com/):
  - `content`: posts
  - `attila`: theme
  - `plugins`
  - `blog`: rendered content

To add a new entry to the blog:
- Create a new markdown file/notebook in `content/`
- Render locally: `pelican content` (requires that dependencies are installed, `pip install -r requirements.txt`)
- `git commit && git push`

To subscribe to the blog, the feed is available at https://datapythonista.me/blog/atom.xml
