Title: pandas benchmarking
Tags: pandas

## Introduction

When developing software, in many cases, execution speed and resource usage are key factors.

If you implement a script or simple program, you execute it, and you get the result you want
in a reasonable time, with the resources you have available, things are simple. But when you
are building something more complex, like a web service with thousands or millions of users,
or a library to work with huge amounts of data, then performance is key. This is the case of
pandas, where a function being two orders of magnitude slower can make the library not usable
by some users, significantly increase the hardware bill of others, and waste time of thousands
of users waiting for their results when executing a Jupyter notebook cell.

How to make software faster is a huge area, and not the topic of this article. The topic of
this article is how to measure resource usage and in particular execution speed of small fragments
of code, that in general capture how fast is a particular function or algorithm This is known
as benchmarking, each individual code to monitor is known as a benchmark.

At the time of writing this post, pandas has a benchmark suite with 1,090 benchmarks. They are
written in Python, and they are executed using [airspeed velocity](https://asv.readthedocs.io/en/stable/)
(or `asv` for short). The main goal is to keep a history, and detect performance regressions.
Instances, where some changes in the pandas source code made certain functionality significantly
slower to users. In an ideal case, we would like to detect if a regression exists before changes
are made, as part of our Continuous Integration tests. But our benchmark suite can take more than
5 hours to execute in a laptop, and more than that in a CI worker. So, it is not feasible at this
point. This article is part of the research to make it happen.

If you analyze the numbers, running 1,090 benchmarks in more than 5 hours (depending on the
computer specs of course), implies that each benchmark is taking around 20 seconds in average.
Our benchmarks are not really that slow. Actually out of the 1,090 benchmark, less than 10
run in more than 20 seconds, and none of them takes more than one minute. The reason for
requiring 20 seconds per benchmark in average is that we need to run them multiple times. What
asv does is complex and not fully clear to me, but seems like every benchmarks is running 10
times by default, and depending on certain things, it will repeat the 10 iterations several
times. In average, seems like every benchmark in the pandas suite is executed around 100 times.

So, why do we need to execute benchmarks so many times? The reason is that executing the same
program several times in the same machine, may seem deterministic, but it is not.
Imagine you are measuring the execution time of a program, once with an idle server, but
another time with many other programs running and competing to get some CPU. Or imagine your
program downloads things from the Internet, and you are affected by the connection speed.

## Benchmark inestability factors

There are actually more factors than what one would probably expect. The main categories are
explained in this section.

### Randomness in execution

One of the most obvious, but probably less relevant factors for benchmarks running faster
or slower is if the input of the benchmark changes. See this simple example:

```python
import random

def my_benchmark():
    for i in range(random.randint(1, 100)):
        do_something()
```

If we time this benchmark, it would clearly give different results at each execution. And if
we use this benchmark for every change in the source code, it would be impossible to tell if
the 100x increase in time is caused by our changes to the `do_something` function making it
much slower, or if in the previous execution the random number generated was 1, and now it
has been 100.

The good news is that most benchmarks shouldn't depend on random input, at least that it
significantly affects the execution like the example. And in the case they do, a random
seed should help run the program in a more deterministic way.

Also, assuming we can get rid of all other noise, it should be quite easy to find the
benchmarks that have randomness, since they are the ones that would generate false positives
when trying to identify performance regressions and implement specific fixes.

### Hardware optimizations

Moving to the hardware layer, and always assuming we are going to run the benchmarks in the
same machine (or an identical machine with the same specs), one of the main reasons for noise
are optimizations that modern CPUs have to save energy. What is known as
[CPU dynamic frequency scaling](https://en.wikipedia.org//wiki/Dynamic_frequency_scaling).
CPUs consume energy when they operate at their normal cycles, and reducing the cycle frequency
or fully stopping the CPU can help avoid heat, save battery in mobile devices, or decrease the
electricity bill.

These optimizations depend on the particular CPU. For the popular Intel core hardware, the main
one is probably [Intel Turbo Boost Technology](https://www.intel.com/content/www/us/en/gaming/resources/turbo-boost.html).
The general idea is that the CPU may run at a base frequency of for example 3.60 GHz, but the
hardware may increase it to 5.00 GHz when the system becomes busy. Other technologies may
involve setting a CPU to sleep when the system load is low. In general, those optimizations
are enabled by default, but can be disabled in the BIOS. And in some cases they can be managed
via software.

One important consideration is that while disabling optimizations can increase stability, and
avoid the need of repeating the benchmarks too many times, they may also make benchmarks run
slower. For example, disabling Turbo Boost in may laptop seems to make benchmarks run in
around double of the time. Whether the increase in stability allows to reduce the number of
repetitions to justify the decrease in performance is a topic for another post.

Another hardware optimization that can affect the execution time of our benchmarks is the
[Hyper Threading Technology](https://www.intel.com/content/www/us/en/gaming/resources/hyper-threading.html)
(or HTT). The idea is that a single CPU can have more than one virtual CPU emulated at the
hardware level. This can be seen as multiprocessing, but at the hardware level. And managing
multiple pipelines that will end up in the same physical CPU seems to allow faster execution.
But what is running in the other virtual CPUs of our physical CPU can affect how much CPU we
can actually use.

### Kernel noise

### Memory access noise

### I/O noise

## Metrics

## Benchmarks environment

## Conclusion

## References

- [airspeed velocity](https://asv.readthedocs.io/en/stable/)
- Victor Stinner series on stable benchmarks
  [part 1 (system)](https://vstinner.github.io/journey-to-stable-benchmark-system.html)
  [part 2 (deadcode)](https://vstinner.github.io/journey-to-stable-benchmark-deadcode.html)
  [part 3 (average)](https://vstinner.github.io/journey-to-stable-benchmark-average.html)
- [Linux Kernel Development, Third Edition](https://www.oreilly.com/library/view/linux-kernel-development/9780768696974/)
- SUSE series on CPU isolation
  [part 1](https://www.suse.com/c/cpu-isolation-introduction-part-1/)
  [part 2](https://www.suse.com/c/cpu-isolation-introduction-part-2/)
  [part 3](https://www.suse.com/c/cpu-isolation-introduction-part-3/)
  [part 4](https://www.suse.com/c/cpu-isolation-introduction-part-4/)
  [part 5](https://www.suse.com/c/cpu-isolation-introduction-part-5/)
  [part 6](https://www.suse.com/c/cpu-isolation-introduction-part-6/)
- RedHat series on cgroups
  [part 1](https://www.redhat.com/en/blog/world-domination-cgroups-part-1-cgroup-basics)
  [part 2](https://www.redhat.com/en/blog/world-domination-cgroups-part-2-turning-knobs)
  [part 3](https://www.redhat.com/en/blog/world-domination-cgroups-part-3-thanks-memories)
  [part 4](https://www.redhat.com/en/blog/world-domination-cgroups-part-4-all-ios)
  [part 5](https://www.redhat.com/en/blog/world-domination-cgroups-part-5-hand-rolling-your-own-cgroup)
  [part 6](https://www.redhat.com/en/blog/world-domination-cgroups-part-6-cpuset)
  [part 7](https://www.redhat.com/en/blog/world-domination-cgroups-rhel-8-welcome-cgroups-v2)
  [part 8](https://www.redhat.com/en/blog/world-domination-cgroups-part-8-down-and-dirty-cgroup-v2)
- [ArchWiki on cgroups](https://wiki.archlinux.org/title/Cgroups)
- [Udacity Course on High Performance Computer Architecture](https://www.udacity.com/course/high-performance-computer-architecture--ud007)
- [Udacity Refresher on Advanced OS](https://www.udacity.com/course/gt-refresher-advanced-os--ud098)
- [Intel Turbo Boost Technology](https://www.intel.com/content/www/us/en/gaming/resources/turbo-boost.html)
- [Intel Hyper Threading Technology](https://www.intel.com/content/www/us/en/gaming/resources/hyper-threading.html)
