#!/usr/bin/env python
import pelican_jupyter.markup

AUTHOR = 'Marc Garcia'
SITE_LOGO = '../static/img/profile.png'
SITENAME = 'datapythonista blog'
SITESUBTITLE = 'about me'
SITEURL = '/blog'
OUTPUT_PATH = 'blog'
PATH = 'content'
TIMEZONE = 'Europe/London'
DEFAULT_LANG = 'en'
THEME = 'attila'
HEADER_COVER = '../static/img/bg.jpg'
MARKUP = ('rst', 'md', 'ipynb')
PLUGIN_PATHS = ['plugins']
PLUGINS = ['pelican_jupyter.markup']
IPYNB_MARKUP_USE_FIRST_CELL = True
IGNORE_FILES = ['.ipynb_checkpoints']
CSS_OVERRIDE = ['../static/css/blog.css']
JS_OVERRIDE = ['../static/js/blog.js']
DISQUS_SITENAME = 'datapythonista'
FEED_DOMAIN = SITEURL
CATEGORY_FEED_ATOM = None
TAG_FEED_ATOM = 'feeds/{slug}.atom.xml'

DEFAULT_PAGINATION = 10
