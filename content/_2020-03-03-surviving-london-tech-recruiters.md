Title: Surviving London tech recruiters

Hi reader, I hope you're well.

![](/static/img/blog/sales_bell.jpeg)

It's been around five years since I started talking to tech recruiters from
London, and more often than not, the experience hasn't been great. I learned
a lot in this time on how things work, and how to deal with recruiters in a
way that doesn't consume all your time and energy. This blog post explains
all what I learned, and hopefully it's useful to candidates of tech roles
that otherwise would have to learn these things the hard way.

I am a contractor Python data engineer/scientist based in London. And this
is based on my personal experience. I guess it should apply to most sofware
related jobs in the UK, but I don't really know.

# Some context

I think when I first moved to London I expected recruiters to be kind of
advisors with lots of positions. And that their job was to help you find
the best one for you. From time to time I see some posts on LinkedIn, like
[this one](https://www.linkedin.com/posts/sallyandrewsmercer_recruiters-are-your-friends-recently-i-met-activity-6640508703252455424-Yy_x)
where it says how recruiters are your friends, and you should build long
term relationships with them, since you'll need them in the future.

Far from reality. The first thing to be aware of is that tech recruiters
are not technical, and surely not advisors. They are sales people. And
their main job is to sell their services to companies looking for candidates.

And closing a deal is going to be their main challenge. This [post](https://www.linkedin.com/posts/sallyandrewsmercer_recruiters-are-your-friends-recently-i-met-activity-6640508703252455424-Yy_x)
should give an idea on how often that happens, and how important that is.

Then, the final part is usually just publishing a job offer in LinkedIn
or similar, or send messages to profiles in LinkedIn.

# Fake offers

One of the main things you want to be aware of are fake offers, and recruiters
without positions. You will end up wasting huge amounts of times otherwise.

As I said, the main job of a recruiter is to look for clients that hire their
services. And this has two main implications for candidates.

The first is that many offers will be very secretive. It's rare to see a job
offer from an agency with the name of the company hiring. And even when receiving
LinkedIn message from recruiters you'll get something like "a leading financial
services company" than the actual name. Of course you'll end up knowing three
emails later, but it's a bit annoying.

The second main implication, is that recruiters will try to use you to find
clients when they don't have one. They will publish fake offers, or contact
you to "discuss your situation". And they'll try to get you into a call as
soon as possible. They'll ask you the normal questions as if they were
recruiting for one or more positions, and probably get you into a database.
But the key part comes at the end, when they ask you "when are you available,
and if you are interviewing with other companies". And of course, they'll want
to know for which companies are you interviewing, and even who are the people
you're talking to. In most cases that's the end of the conversation for you,
you won't hear back from them, and they'll go on with their job of looking
for clients, with the information you provided them.

You can easily be wasting several hours per day in the phone with recruiters
if you don't try to avoid these calls.

From my experience, recruiters with genuine offers are always happy to
provide a decent amount of information about the position over LinkedIn or
email. Recruiters who need to discuss them on the phone are usually time
wasters. If you insist in knowing more details, they can even send a fake
job description, with just generic things like "a company that is disrupting
their industry". They should also be able to provide you with a rate or
salary range.

What I do is to always ask how much a position pays first. This is of course
useful to know whether the position can be of interest or not. But I think
it also gives you good information to know if there is an actual position
to discuss.

# Job titles

![](/static/img/blog/impersonate.jpeg)

Another thing worth being aware of is to know who you are speaking with.
I think it's very rare to see anyone on LinkedIn with the title
"Recruiter specialized in Python roles". Usually, the titles are
(I guess intentionally) misleading. Something like "Big data consultant",
"Python consultant" or even "Open source consultant". That makes you think
they are actually offering consulting services in big data, Python or open
source.

When they don't do that, they'll usually use titles like "Director", "Leader",
"Principal"... I guess candidates are more open to talk to people when they
are technical professionals or senior managers than when they are recruiters.
So, recruiters will use all those fancy titles, but they are still recruiters.

# Negotiating your salary

Talking about money is often tricky, and worth knowing how things work. In
general, there are two different ways recruitment agencies will work with
their clients.

The first, you need to negotiate your rate or salary, and the recruitment
agency will apply a markup on top of it.

The second, the company will pay the salary a rate, and the agency will take
a margin on that, offering you a lower rate.

There is a big difference being in one or the other. When you're on the first
one, the recruiter is your friend and can help you get a rate as high as possible.
If you are going to work at £500 per day, and they agreed a markup of 10%
they will receive £50 per day. But if you can get £600, they will get £60, so
they will be even happier with that. So, the actual negotiation is with the
hiring company, and the recruiter can provide insights on what other candidates
ask, what's too much, or what's too low. The final decision on how much to ask
is still yours, but they will help you.

The second option is very different. Let's say a company is happy to pay £700 daily
to a recruitment agency for a data engineer position. Then, the agency may offer
you £500, applying a 20% markup, and get more than their fair share from the company
rate.

What is a fair share is not clear, but applying around a 15% to your contractor rate 
is expected. Note that this is different from applying a margin to the rate the
hiring company is paying. For example, for a contract where the contractor gets £400
and the company is paying £500, the company is applying a 25% markup to the £400,
or is taking a 20% margin of the £500. Both options are the same, as the agency
is getting £100, but the percentage is different depending on whether it's a markup
to the contractor rate, or a margin on the gross rate.

<iframe width="560" height="315" src="https://www.youtube.com/embed/yJqwVGqHMqc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Contractor admin stuff

Another thing to consider is that technically speaking, your client as a contractor
will be the agency, and not the company where you will be working. This means that
the company receiving your invoices and paying then is the agency. I used to work
with very serious agency in the past, but recently I was about to sign a contract
with one where they felt like being 20 years ago in technology. In general, agencies
will have some sort of intranet to fill timesheets, and most stuff will be automated.
Including self-billing (they will give you the invoices you need to send them).
To sign the contract you will be using some platform to sign electronically, so you
don't need to print the contract, sign it, scan it, and send it back.

But apparently there are agencies that expect you to send the worked hours in an Excel
via email every month, and sign the contract manually. The agency I was talking to
also had a not very specific payment scheme. Most agencies have a calendar to know
exactly when you are being paid, but in some cases they don't. And you may get
delayed payments without much accountability for them.

# Being contacted by recruiters

If you are in data science, data engineering, software engineering, or similar roles,
but you don't think the previous applies to you, because you rarely get contacted by
recruiters, then you may want to consider this.

I think the main platform for finding a job in 2020 is by far LinkedIn. And LinkedIn
works in a quite simple way, very similar to an old search engine. When you complete
your profile, you are asked for job titles, job descriptions, skills... With all the
words in your profile, LinkedIn will check how many of each are there. Imagine that
in my profile I've got 30 times Python, 25 data, 20 science... Then, a recruiter
is looking for profiles with the search "Python data scientist". Since I've got
many times does keywords, more than most people, I'll be in the top of their
results.

That's true for most cases, but there are couple of other factors. Obviously the
location. I need to be where they are looking at. And second and a bit more complex,
I need to be in their network. And their network means that we need to be connected,
have a connection in common, or have a connection each that are connected among them
(3rd level connection). If you're not in this network, you will be returned in
searches looking by your name, but not when someone is looking for skills.

So, if you want to receive messages from lots of recruiters (not saying you really
want), you want to have a big network. Try to connect with people in your industry,
recruiters, and anyone you can. The more connections they have (recruiters have plenty)
the better. And make sure you have all the right keywords in your profile.
